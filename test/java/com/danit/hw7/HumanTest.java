package myhomework7.test.java.com.danit.hw7;
import myhomework7.test.java.com.danit.hw7.*;
import myhomework7.main.java.com.danit.hw7.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human john = new Man("John","Wick",1980);
    Human human = new Man();

    @Test
    void testToString() {
        String actual = john.toString();
        String expected = "Human {name = " + john.getName() + ", " + "surname = " + john.getSurname() + ", " + "year = " + john.getYear() + ", "  + "iq = " + john.getIq() + ", " + john.getSchedule() + "}";
        assertEquals(expected,actual);
    }

    @Test
    void testHashcode(){
        assertTrue(human.hashCode()!=john.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(john.equals(human));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(john.equals(john));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(human.equals(john),john.equals(human));
    }

    @Test
    void testTransitivity() {
        Human human2 = new Man();
        boolean[] test = {john.equals(human2),human.equals(human2),john.equals(human2)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = john.equals(human);
        boolean secondInvoke = john.equals(human);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,john.equals(null));
    }
}