package myhomework7.main.java.com.danit.hw7;

import java.util.HashMap;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<DayOfWeek,String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    public void repairCar() {
        System.out.println("I need to repair my car");
    }

    @Override
    public void greetPet() {
            System.out.println("Привет, " + super.getFamily().getPet().getNickname() + "!");
    }
}
