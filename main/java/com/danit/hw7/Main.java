package myhomework7.main.java.com.danit.hw7;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        //first family
        Man john = new Man("John","Wick",1980);
        john.setIq(150);
        HashMap<DayOfWeek,String> scheduleJohn = new HashMap<DayOfWeek,String>();
        scheduleJohn.put(DayOfWeek.SATURDAY,"Play football");
        scheduleJohn.put(DayOfWeek.SUNDAY,"Goto the gym");
        john.setSchedule(scheduleJohn);
        Woman karla = new Woman("Karla","Bruni",1980);
        karla.setIq(150);
        HashMap<DayOfWeek,String> scheduleKarla = new HashMap<DayOfWeek,String>();
        scheduleKarla.put(DayOfWeek.SATURDAY,"Go shopping");
        scheduleKarla.put(DayOfWeek.SUNDAY,"Play tennis");
        karla.setSchedule(scheduleKarla);
        Family familyWick = new Family(karla, john);
        Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
        dog.getSpecies().setNumberOfPaw(4);
        dog.getSpecies().setHasFur(true);
        dog.getSpecies().setCanFly(false);
        familyWick.setPet(dog);
        Woman natali = new Woman("Natali","Portman",1990,97,scheduleKarla);
        familyWick.addChild(natali);
        natali.setFamily(familyWick);
        Woman liza = new Woman("Liza","Mineli",2008,109,scheduleJohn);
        familyWick.addChild(liza);
        liza.setFamily(familyWick);

        //second family
        Man arnold = new Man("Arnold","Schwarzenegger",1960);
        arnold.setIq(200);
        HashMap<DayOfWeek,String> scheduleArnold = new HashMap<DayOfWeek,String>();
        scheduleArnold.put(DayOfWeek.SATURDAY,"Go to the gym");
        scheduleArnold.put(DayOfWeek.SUNDAY,"Go to the gym");
        arnold.setSchedule(scheduleArnold);
        Woman monica = new Woman("Monica","Belucci",1962);
        monica.setIq(150);
        HashMap<DayOfWeek,String> scheduleMonica = new HashMap<DayOfWeek,String>();
        scheduleMonica.put(DayOfWeek.SATURDAY,"Visit Jennifer");
        scheduleMonica.put(DayOfWeek.SUNDAY,"Go to the bar");
        monica.setSchedule(scheduleMonica);
        Family familyArnold = new Family(monica, arnold);
        DomesticCat cat = new DomesticCat("Olaf",4,50, new HashSet<String>(Set.of("sleep","eat")));
        cat.getSpecies().setCanFly(false);
        cat.getSpecies().setHasFur(true);
        cat.getSpecies().setNumberOfPaw(4);
        familyArnold.setPet(cat);
        Woman michel = new Woman("Michel","Schwarzeneger",1992,37,scheduleMonica);
        familyArnold.addChild(michel);
        michel.setFamily(familyArnold);

        System.out.println(familyArnold);
        System.out.println(familyWick);

        //вызов методов ребёнка second family
        System.out.println(michel);
        michel.describePet();
        michel.greetPet();
        michel.feedPet(false);

        //вызов методов Pet
        System.out.println(cat);
        cat.eat();
        cat.respond();

        //вызов метода delete child через human
        System.out.println(familyWick.getChildren());
        familyWick.deleteChild(natali);
        System.out.println(familyWick.getChildren());
        familyArnold.deleteChild(liza);
        cat.foul();
        john.repairCar();
        monica.makeup();
        monica.setFamily(familyArnold );
        Human human = monica.bornChild();
        System.out.println(human);
        System.out.println(dog);
        HashSet<Pet> wikcsPets = new HashSet<Pet>();
        System.out.println(wikcsPets);
        wikcsPets.add(dog);
        System.out.println(wikcsPets);
        familyWick.setPetsSet(wikcsPets);
        wikcsPets.add(new Fish("Solomon",1,20,new HashSet()));
        System.out.println(familyWick.getPetsSet());





//        int count = 0;
//        while (count < 10000000) {
//            Man man = new Man();
//        }




    }



}
