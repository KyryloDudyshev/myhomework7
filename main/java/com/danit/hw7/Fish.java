package myhomework7.main.java.com.danit.hw7;

import java.util.HashSet;

public class Fish extends Pet {
    Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, HashSet habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.getNickname() + "." + " Я соскучился!");
    }
}
